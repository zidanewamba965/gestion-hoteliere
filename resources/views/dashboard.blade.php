<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../../css/font-awesome.min.css">
	<link rel="stylesheet" href="../../css/templatemo_misc.css">
	<link rel="stylesheet" href="../../css/templatemo_style.css">
    <title>{{ Auth::user()->name }}</title>
</head>
<body>
    
<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ Auth::user()->email }}
        </h2>
    </x-slot>

    @if(Session::has('success'))
    <div class="alert alert-success">
        {{Session::get('success')}}
    </div>
@endif
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                <a href="/listehotel">Nouvelle reservation chambre</a>   
                </div>
            </div>
            <h2>Liste de Reservation</h2>
            <?php	

            foreach($fairereservations as $fairereservation){

            if($fairereservation->type_chambre == 'Single Room Deluxe'){ 
                $prix_single_room_deluxe = 35000;
            }

            if($fairereservation->type_chambre == 'Single Room'){
                $prix_single_room = 25000;
                
            }
            
            if($fairereservation->type_chambre == 'Double Room'){ 
                $prix_double_room = 40000;
                
            }
            
            if($fairereservation->type_chambre == 'Double Room Deluxe'){
                $prix_double_room_deluxe = 50000;
                
            }
 }	
?>

<table>
@foreach($fairereservations as $fairereservation)
     
<thead>
        <th>Id reser</th>
        <th>Nom</th>
        <th>Nom hotel</th>
        <th>Type chambre</th>
        <th>Nombre de personne</th>
        <th>Numero de téléphone</th>
        <th>Sejour</th>
        <th>date d'arrivée</th>
        <th>Mode de Payement</th>
        <th>Total à payer</th>
        <th>Action</th>
    </thead> 
   
    <br> <br> <br>
    <td>{{$fairereservation->id}}</td>
    <td>{{$fairereservation->name}}</td>
    <td>{{$fairereservation->nom_hotel}}</td>
    <td>{{$fairereservation->type_chambre}}</td>
    <td>{{$fairereservation->nombre_personne}}</td>
    <td>{{$fairereservation->num_tel}}</td>
    <td>{{$fairereservation->duree_sejour}}</td>
    <td>{{$fairereservation->date_arrivee}}</td>
    <td>{{$fairereservation->mode_payement}}</td>
    <td><a href="{{url('fairereservation/editreservation/'.$fairereservation->id)}}"><button type="submit">Modifier</button></a></td> 

  
        @if($fairereservation->type_chambre == 'Single Room Deluxe')
            
            {{$final = $prix_single_room * $fairereservation->duree_sejour}}
        @endif
        @if($fairereservation->type_chambre == 'Single Room')
            
            {{$final = $prix_single_room * $fairereservation->duree_sejour}}
        @endif
        @if($fairereservation->type_chambre == 'Double Room') 
            
            {{$final = $prix_double_room * $fairereservation->duree_sejour}}
        @endif
        @if($fairereservation->type_chambre == 'Double Room Deluxe')
            
            {{$final = $prix_double_room_deluxe * $fairereservation->duree_sejour}}
        @endif
    @endforeach
</table>
        </div>
    </div>
</x-app-layout>

    
</body>
</html>