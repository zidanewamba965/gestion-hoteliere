<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="" />
	<meta name="description" content="" />
<!-- 

Sonic Template 

http://www.templatemo.com/tm-394-sonic 

-->
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../../css/bootstrap.min.css">
	<link rel="stylesheet" href="../../css/font-awesome.min.css">
	<link rel="stylesheet" href="../../css/templatemo_misc.css">
	<link rel="stylesheet" href="../../css/templatemo_style.css">
    <title>Reservation {{Request('nom_hotel')}}</title>
</head>
<body>
    
    
	
	<!-- This one in here is responsive menu for tablet and mobiles -->
    <div class="responsive-navigation visible-sm visible-xs">
        <a href="#" class="menu-toggle-btn">
            <i class="fa fa-bars fa-2x"></i>
        </a>
        <div class="navigation responsive-menu">
            <ul>
                <li class="home"><a href="/">Home</a></li>
	            <li><a href="#" class="external">Reservation</a></li>
            </ul> <!-- /.main_menu -->
        </div> <!-- /.responsive_menu -->
    </div> <!-- /responsive_navigation -->

	<div id="main-sidebar" class="hidden-xs hidden-sm">
		<div class="logo">
			<a href="#"><h1>{{request('nom_hotel')}}</h1></a>
			<span>Faites des reservations</span>
		</div> <!-- /.logo -->

		<div class="navigation">
	        <ul class="main-menu">
	            <li class="home"><a href="/">Home</a></li>
	            <li><a href="#" class="external">Reservation</a></li>
	        </ul>
		</div> <!-- /.navigation -->

	</div> <!-- /#main-sidebar -->

	<div id="main-content">

		<div id="templatemo">
			<div class="main-slider">
				<div class="flexslider">
					<ul class="slides">
                    
						<li>
							<div class="slider-caption">
								<h2>Hilton Hôtel</h2>
								<p>5 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Read More</a>
							</div>
							<img src="images/hilton3.jpg" alt="Slide 1">
						</li>
                        
						<li>
							<div class="slider-caption">
								<h2>Marriot Hôtel</h2>
								<p>4 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Details</a>
							</div>
							<img src="images/marriot2.jpg" alt="Slide 2">
						</li>
                        
                        <li>
							<div class="slider-caption">
								<h2>Adys Hôtel</h2>
								<p>4 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Downloads</a>
							</div>
							<img src="images/hotel-3-etoiles.jpg" alt="Slide 3">
						</li>
                        
					</ul>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="welcome-text">
							<h2>reservation sur <b>{{request('nom_hotel')}}</b></h2>
							<p>{{request('description_hotel')}}</p>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- /#sTop -->

        
			<div id="contact" class="section-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>Reservation</h2>
						</div> <!-- /.section-title -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
				
				<div class="row contact-form">
					<div class="col-md-4">
                    
                        <form action="" method="post">
                            
                          
						<label for="type_chambre">Type Chambre:</label>
						<select class="form-control" name="type_chambre">
                            <option value=""></option>
                            <option value="">Single Room</option>
                            <option value="">Double Room</option>
                            <option value="">Single Room Deluxe</option>
                            <option value="">Double Room Deluxe</option>
                        </select>
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
						<label for="type_suite">Type Suite:</label>
						<select class="form-control" name="type_suite">
                            <option value=""></option>
                            <option value="">Honey Suite Deluxe</option>
                            <option value="">Honey Suite</option>
                        </select>
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
						<label for="type_salle">Type Salle:</label>
						<select class="form-control" name="type_salle">
                            <option value=""></option>
                            <option value="">Salle Conférence</option>
                            <option value="">Salle Spectacle</option>
                            <option value="">Salle Reunion</option>
                        </select>
					</div> <!-- /.col-md-4 -->
                 
				</div>

                <div class="row contact-form">
					<div class="col-md-4">
					

                    <label for="nombre_personne" class="required">Nombre Personne:</label>
						<input class="form-control" name="nombre_personne">
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
                    <label for="num_tel" class="required">Numero Telephone:</label>
						<input class="form-control" name="num_tel">
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
                    <label for="duree_sejour" class="required">Durée Dejour:</label>
						<input class="form-control" name="duree_sejour">
					</div> <!-- /.col-md-4 -->
				</div>

                <div class="row contact-form">
					<div class="col-md-4">
						<label for="date_arrivee" class="required">Date Arrivée:</label>
						<input class="form-control" type="date" name="date_arrivee">
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
                    <label for="mode_payement" class="required">Mode Payement:</label>
                    <select class="form-control" name="type_salle">
                            <option value="orange" name=""></option>
                            <option value="orange" name="orange_money">Orange Money</option>
                            <option value="mtn" name="mobile_money">Mobile Money</option>
                    </select>
					</div> <!-- /.col-md-4 -->
				</div>
                <div class="col-md-12">
						<div class="submit-btn">
							<a href="/login" class="largeButton contactBgColor">Reserver</a>
						</div> <!-- /.submit-btn -->
				</div> <!-- /.col-md-12 -->
                    </div>


                        </form>

                    </div>
                </div>
			</div> <!-- /#contact -->

    </div>


</body>
</html>