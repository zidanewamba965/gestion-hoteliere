<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="" />
	<meta name="description" content="" />
<!-- 

Sonic Template 

http://www.templatemo.com/tm-394-sonic 

-->
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../../css/bootstrap.min.css">
	<link rel="stylesheet" href="../../css/font-awesome.min.css">
	<link rel="stylesheet" href="../../css/templatemo_misc.css">
	<link rel="stylesheet" href="../../css/templatemo_style.css">
    <title>Reservation {{Request('nom_hotel')}}</title>
</head>
<body>
    
    
	
	<!-- This one in here is responsive menu for tablet and mobiles -->
    <div class="responsive-navigation visible-sm visible-xs">
        <a href="#" class="menu-toggle-btn">
            <i class="fa fa-bars fa-2x"></i>
        </a>
        <div class="navigation responsive-menu">
            <ul>
                <li class="home"><a href="/">Home</a></li>
	            <li><a href="#" class="external">Reservation</a></li>
            </ul> <!-- /.main_menu -->
        </div> <!-- /.responsive_menu -->
    </div> <!-- /responsive_navigation -->

	<div id="main-sidebar" class="hidden-xs hidden-sm">
		<div class="logo">
			<a href="#"><h1>{{request('nom_hotel')}}</h1></a>
			<span>Faites des reservations</span>
		</div> <!-- /.logo -->

		<div class="navigation">
	        <ul class="main-menu">
	            <li class="home"><a href="/">Home</a></li>
	            <li><a href="#" class="external">Reservation</a></li>
	        </ul>
		</div> <!-- /.navigation -->

	</div> <!-- /#main-sidebar -->

	<div id="main-content">

		<div id="templatemo">
			<div class="main-slider">
				<div class="flexslider">
					<ul class="slides">
                    
						<li>
							<div class="slider-caption">
								<h2>Hilton Hôtel</h2>
								<p>5 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Read More</a>
							</div>
							<img src="images/hilton3.jpg" alt="Slide 1">
						</li>
                        
						<li>
							<div class="slider-caption">
								<h2>Marriot Hôtel</h2>
								<p>4 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Details</a>
							</div>
							<img src="images/marriot2.jpg" alt="Slide 2">
						</li>
                        
                        <li>
							<div class="slider-caption">
								<h2>Adys Hôtel</h2>
								<p>4 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Downloads</a>
							</div>
							<img src="images/hotel-3-etoiles.jpg" alt="Slide 3">
						</li>
                        
					</ul>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="welcome-text">
							<h2>Vous voulez reservez sur quel Hôtel</b></h2>
							@foreach($hotels as $hotel)
                            <a href="{{url('fairereservation/'.$hotel->nom_hotel)}}"><p>{{$hotel->nom_hotel}}</p></a>
                            @endforeach
						</div>
					</div>
				</div>
			</div>
		</div> <!-- /#sTop -->

    </div>


</body>
</html>