<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="" />
	<meta name="description" content="" />
<!-- 

Sonic Template 

http://www.templatemo.com/tm-394-sonic 

-->
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="../../css/font-awesome.min.css">
	<link rel="stylesheet" href="../../css/templatemo_misc.css">
	<link rel="stylesheet" href="../../css/templatemo_style.css">
    <title>Reservation {{Request('nom_hotel')}}</title>
</head>
<body>
    
    
	
	<!-- This one in here is responsive menu for tablet and mobiles -->
    <div class="responsive-navigation visible-sm visible-xs">
        <a href="#" class="menu-toggle-btn">
            <i class="fa fa-bars fa-2x"></i>
        </a>
        <div class="navigation responsive-menu">
            <ul>
                <li class="home"><a href="/">Home</a></li>
	            <li><a href="#" class="external">Reservation</a></li>
            </ul> <!-- /.main_menu -->
        </div> <!-- /.responsive_menu -->
    </div> <!-- /responsive_navigation -->

	<div id="main-sidebar" class="hidden-xs hidden-sm">
		<div class="logo">
			<a href="#"><h1>{{request('nom_hotel')}}</h1></a>
			<span>Faites des reservations</span>
		</div> <!-- /.logo -->

		<div class="navigation">
	        <ul class="main-menu">
	            <li class="home"><a href="/">Home</a></li>
	            <li><a href="#" class="external">Reservation</a></li>
	        </ul>
		</div> <!-- /.navigation -->

	</div> <!-- /#main-sidebar -->

	<div id="main-content">

		<div id="templatemo">
			<div class="main-slider">
				<div class="flexslider">
					<ul class="slides">
                    
						<li>
							<div class="slider-caption">
								<h2>Hilton Hôtel</h2>
								<p>5 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Read More</a>
							</div>
							<img src="images/hilton3.jpg" alt="Slide 1">
						</li>
                        
						<li>
							<div class="slider-caption">
								<h2>Marriot Hôtel</h2>
								<p>4 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Details</a>
							</div>
							<img src="images/marriot2.jpg" alt="Slide 2">
						</li>
                        
                        <li>
							<div class="slider-caption">
								<h2>Adys Hôtel</h2>
								<p>4 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Downloads</a>
							</div>
							<img src="images/hotel-3-etoiles.jpg" alt="Slide 3">
						</li>
                        
					</ul>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="welcome-text">
							<h2>reservation sur <b>{{request('nom_hotel')}}</b></h2>
							<p>{{request('description_hotel')}}</p>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- /#sTop -->

        
			<div id="contact" class="section-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>Reservation</h2>
						</div> <!-- /.section-title -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
				
				<div class="row contact-form">
					<div class="col-md-4">
                    
                        <form action="{{url('fairereservation/'.request('nom_hotel'))}}" method="POST" enctype="multipart/form-data">
                            
                        @csrf
						<input type="hidden" name="email" value="{{ Auth::user()->email }}">
						<input type="hidden" name="name" value="{{ Auth::user()->name }}">
						<input type="hidden" name="nom_hotel" value="{{request('nom_hotel')}}">
						<div class="form-group">  
							<label for="type_chambre" class="required">Type Chambre:</label>
								<select class="form-control prc" name="type_chambre" required>
                           		 <option value=""></option>
                            	<option value="Single Room">Single Room</option>
                            	<option value="Double Room">Double Room</option>
                          		<option value="Single Room Deluxe">Single Room Deluxe</option>
                            	<option value="Double Room Deluxe">Double Room Deluxe</option>
                        		</select>
					</div>
					</div> <!-- /.col-md-4 -->
						               
				</div>

                <div class="row contact-form">
					<div class="form-group"> 
                    	<label for="nombre_personne" class="required">Nombre Personne:</label>
						<input type="number" class="form-control prc" name="nombre_personne" value="1" required>
					</div> <!-- /.col-md-4 -->
					<!-- /.col-md-4 -->
					<div class="form-group">
                    <label for="duree_sejour" class="required">Durée Dejour:</label>
						<input type="number" class="form-control prc" name="duree_sejour" value="1" required>
					</div> <!-- /.col-md-4 -->
				</div>

                <div class="row contact-form">
					<div class="form-group">
						<label for="date_arrivee" class="required">Date Arrivée:</label>
						<input class="form-control prc" type="date" name="date_arrivee" required>
					</div> <!-- /.col-md-4 -->

					<div class="form-group">
						<label for="num_phone" class="required">Numero Telephone:</label>
						<input class="form-control prc" type="number" name="num_tel" required>
					</div> <!-- /.col-md-4 -->

					<div class="form-group">
					
                    <label for="mode_payement" class="required">Mode Payement:</label>
                    <select class="form-control prc" name="mode_payement" required>
                            <option value=""></option>
                            <option value="orange money">Orange Money</option>
                            <option value="mtn mobile money">MTN Mobile Money</option>
                    </select>
					</div>
				
					<!-- /.col-md-4 -->
				</div>
                <div class="col-md-12 form-group">
						<div class="submit-btn">
							<button>Reserver</button>
						</div> <!-- /.submit-btn -->

						<div class="form-group">
                    <div class="price-lab">
                        TOTAL: <label><output class="price-fi" id="price-count"></output></label>$
                    </div>
                </div>
				</div> <!-- /.col-md-12 -->
                    </div>

								@if($errors->any())
									@foreach($errors->all() as $error)
										<div class="text-red-500">{{$error}}</div>
									@endforeach
								@endif
                        </form>
					
                    </div>
                </div>
			</div> <!-- /#contact -->
			

    </div>
	


	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/jquery.singlePageNav.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	<script>
		$(document).ready(function(){
			$("a[data-gal^='prettyPhoto']").prettyPhoto({hook: 'data-gal'});
		});

        function initialize() {
          var mapOptions = {
            zoom: 13,
            center: new google.maps.LatLng(40.7809919,-73.9665273)
          };

          var map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);
        }

        function loadScript() {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
              'callback=initialize';
          document.body.appendChild(script);
        }

        window.onload = loadScript;
    </script>

	<script>
		// process live cost price of reservation form

$('.form-group').on('input', '.prc', function () {

var totalPrice = 0;
var counter = 0;

$('.form-group .prc').each(function () {
  var inputVal = $(this).val();

  if (inputVal.includes("Single Room")) {
	totalPrice = 200;
  }

  if (inputVal.includes("Double Room")) {
	totalPrice = 300;
  }

  if (inputVal.includes("Family")) {
	totalPrice = 400;
  }

  if (inputVal.includes("Honeymoon Suite Deluxe")) {
	totalPrice = 500;
  }

  if (inputVal.includes("Honeymoon Suite")) {
	totalPrice = 500;
  }

  if (inputVal.includes("Suite")) {
	totalPrice += 200;
  }

  if (inputVal.includes("Deluxe")) {
	totalPrice += 200;
  }

  if (inputVal.includes("YES")) {
	totalPrice += 200;
  }

  if ($.isNumeric(inputVal)) {
	if (counter == 1) {
	  totalPrice += (inputVal * 80);
	}

	if (counter == 2) {
	  totalPrice += (inputVal * 400);
	}

	if (counter == 3) {
	  totalPrice += (inputVal * 60);
	}

	if (counter == 4) {
	  totalPrice += (inputVal * 10);
	}
  }

  counter++;
});

document.getElementById('priceField').value = totalPrice;
$('#price-count').text(totalPrice);

});
	</script>

</body>
</html>
