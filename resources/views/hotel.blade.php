<!DOCTYPE html>
<html>
<head>
    <title>{{request('nom_hotel')}}</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
<!-- 

Sonic Template 

http://www.templatemo.com/tm-394-sonic 

-->
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../../css/bootstrap.min.css">
	<link rel="stylesheet" href="../../css/font-awesome.min.css">
	<link rel="stylesheet" href="../../css/templatemo_misc.css">
	<link rel="stylesheet" href="../../css/templatemo_style.css">
</head>
<body>
	
	<!-- This one in here is responsive menu for tablet and mobiles -->
    <div class="responsive-navigation visible-sm visible-xs">
        <a href="#" class="menu-toggle-btn">
            <i class="fa fa-bars fa-2x"></i>
        </a>
        <div class="navigation responsive-menu">
            <ul>
                <li class="home"><a href="#templatemo">Home</a></li>
	            <li class="about"><a href="#about">Chambres</a></li>
	            <li class="services"><a href="#services">Suites</a></li>
	            <li class="portfolio"><a href="#portfolio">Activites</a></li>
	            <li class="contact"><a href="{{url('forum/'.request('nom_hotel'))}}" class="external">Forum</a></li>
	            <li><a href="/login" class="external">Reservation</a></li>
            </ul> <!-- /.main_menu -->
        </div> <!-- /.responsive_menu -->
    </div> <!-- /responsive_navigation -->

	<div id="main-sidebar" class="hidden-xs hidden-sm">
		<div class="logo">
			<a href="#"><h1>{{request('nom_hotel')}}</h1></a>
			<span>Faites des reservations</span>
		</div> <!-- /.logo -->

		<div class="navigation">
	        <ul class="main-menu">
	            <li class="home"><a href="#templatemo">Home</a></li>
	            <li class="about"><a href="#about">Chambres</a></li>
	            <li class="services"><a href="#services">Suites</a></li>
	            <li class="portfolio"><a href="#portfolio">Activites</a></li>
	            <li class="contact"><a href="{{url('forum/'.request('nom_hotel'))}}" class="external">Forum</a></li>
	            <li><a href="/login" class="external">Reservation</a></li>
	        </ul>
		</div> <!-- /.navigation -->

	</div> <!-- /#main-sidebar -->

	<div id="main-content">

		<div id="templatemo">
			<div class="main-slider">
				<div class="flexslider">
					<ul class="slides">
                    
						<li>
							<div class="slider-caption">
								<h2>Hilton Hôtel</h2>
								<p>5 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Read More</a>
							</div>
							<img src="../../images/hilton3.jpg" alt="Slide 1">
						</li>
                        
						<li>
							<div class="slider-caption">
								<h2>Marriot Hôtel</h2>
								<p>4 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Details</a>
							</div>
							<img src="../../images/marriot2.jpg" alt="Slide 2">
						</li>
                        
                        <li>
							<div class="slider-caption">
								<h2>Adys Hôtel</h2>
								<p>4 Etoiles Cameroun</p>
								<a href="#" class="largeButton homeBgColor">Downloads</a>
							</div>
							<img src="../../images/hotel-3-etoiles.jpg" alt="Slide 3">
						</li>
                        
					</ul>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="welcome-text">
							<h2>Bienvenue sur <b>{{request('nom_hotel')}}</b></h2>
							<p>{{request('description_hotel')}}</p>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- /#sTop -->

		<div class="container-fluid">

			<div id="about" class="section-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>Nos Chambres</h2>
						</div>
					</div>
				</div>
				<div class="row">
    
                    @foreach($chambres as $chambre)
					@if($chambre->nom_hotel==request('nom_hotel'))

					<div class="col-md-4">
						<div class="member-item">
							<div class="member-thumb">
								<a href="./../images/chambres/{{$chambre->nom_photo_chambre}}.jpg" ><img src="../../images/chambres/{{$chambre->nom_photo_chambre}}.jpg" alt="{{request('nom_photo_image')}}" width="150px"></a>
							</div>
                
							<div class="member-content">
								<h4><b>{{$chambre->prix_chambre}} Fcfa</b></h4>
								<p><b>{{$chambre->description_chambre}}</b></p>
								<p><a href="/login" role="button" class="btn btn-success">Reserver</a></p>
							</div>
						</div>
					</div> <!-- /.col-md-4 -->
                    
					@endif
					&nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp;
                    @endforeach
				</div> <!-- /.row -->
				<div class="row our-story">
					<div class="col-md-8">
						<h3>Particularités</h3>
					  	<p>Notre Hotel vous offre le meilleur qui soit. Vous pouvez également réserver des salles pour 
                              vos céromonies, des chambres à durée déterminer, des suites pour vos lunes de miels à durée déterminer.
                              Notre Hôtel offre également différents types d'activités pour vos loisir.
                          </p>
					</div>
					<div class="col-md-4">
						<div class="story-image">
							<p><h1>{{request('nom_hotel')}}</h1></p>
						</div>
					</div>
				</div> <!-- /.row -->
			</div> <!-- /#about -->
			
			<div id="services" class="section-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>Suites</h2>
						</div> <!-- /.section-title -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
                
				@foreach($suits as $suit)
				@if($suit->nom_hotel==request('nom_hotel'))
					<div class="col-md-4">
						<div class="member-item">
							<div class="member-thumb">
								<a href="./../images/suites/{{$suit->nom_photo_suite}}" ><img src="../../images/suites/{{$suit->nom_photo_suite}}.jpg" alt="{{request('type_suite')}}"></a>
							</div>
                            
							<div class="member-content">
								<h4>{{$suit->prix_suite}}</h4>
								<p><b><i> {{$suit->type_suite}}</i></b><br>{{$suit->description_suite}}</p>
								<p><a href="/login" role="button" class="btn btn-success">Reserver</a></p>
							</div>
						</div>
					</div> <!-- /.col-md-4 -->
                    &nbsp; &nbsp;
				@endif
                   @endforeach 
			</div> <!-- /#Nos Hotels -->

			<div id="portfolio" class="section-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>Activites</h2>
						</div> <!-- /.section-title -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
				<div class="row">
				@foreach($activites as $activite)
					<div class="col-md-4">
					
						<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="../../images/activite/{{$activite->nom_photo_activite}}.jpg" alt="Activite">
								<div class="overlay-p">
									<a href="images/portfolio/1.jpg" data-gal="prettyPhoto">
										<i class="fa fa-arrows-alt fa-2x"></i>
									</a>
								</div>
							</div> <!-- /.portfolio-thumb -->
							<h3 class="portfolio-title"><a href="#">Activite</a></h3>
							<p>{{$activite->description_activite}}</p>
						</div> <!-- /.portfolio-item -->
					</div> <!-- /.col-md-4 -->
					&nbsp; &nbsp;
					@endforeach
				</div> <!-- /.row -->
				<div class="row">
										
				<div class="row">
					<div class="col-md-12">
						<div class="load-more">
							<a href="#portfolio" class="largeButton portfolioBgColor">Load More</a>
						</div>
					</div>
				</div>
			</div> <!-- /#portfolio -->

			
		<div class="site-footer">
			<div class="first-footer">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="social-footer">
								<ul>
									<li><a href="#" class="fa fa-facebook"></a></li>
									<li><a href="#" class="fa fa-twitter"></a></li>
									<li><a href="#" class="fa fa-dribbble"></a></li>
									<li><a href="#" class="fa fa-linkedin"></a></li>
									<li><a href="#" class="fa fa-rss"></a></li>
								</ul>
							</div> <!-- /.social-footer -->
						</div> <!-- /.col-md-12 -->
					</div> <!-- /.row -->
				</div> <!-- /.container-fluid -->
			</div> <!-- /.first-footer -->
			<div class="bottom-footer">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6">
							<p class="copyright">Copyright © 2021 <a href="#">Gestu Hotel</a>
                            </p>
						</div> <!-- /.col-md-6 -->
						<div class="col-md-6 credits">
							<p><!-- Design: <a rel="nofollow" href="http://www.templatemo.com/tm-394-sonic" target="_parent">Sonic</a> --></p>
						</div> <!-- /.col-md-6 -->
					</div> <!-- /.row -->
				</div> <!-- /.container-fluid -->
			</div> <!-- /.bottom-footer -->
		</div> <!-- /.site-footer -->

	</div> <!-- /#main-content -->

	<!-- JavaScripts -->
	<script src="../../js/jquery-1.10.2.min.js"></script>
	<script src="../../js/jquery.singlePageNav.js"></script>
	<script src="../../js/jquery.flexslider.js"></script>
	<script src="../../js/jquery.prettyPhoto.js"></script>
	<script src="../../js/custom.js"></script>
	<script>
		$(document).ready(function(){
			$("a[data-gal^='prettyPhoto']").prettyPhoto({hook: 'data-gal'});
		});

        function initialize() {
          var mapOptions = {
            zoom: 13,
            center: new google.maps.LatLng(40.7809919,-73.9665273)
          };

          var map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);
        }

        function loadScript() {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
              'callback=initialize';
          document.body.appendChild(script);
        }

        window.onload = loadScript;
    </script>
<!-- templatemo 394 sonic -->
</body>
</html>