<!DOCTYPE html>
<html>
<head>
    <title>Gestu Hotel</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
<!-- 

Sonic Template 

http://www.templatemo.com/tm-394-sonic 

-->
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/templatemo_misc.css">
	<link rel="stylesheet" href="css/templatemo_style.css">
</head>
<body>
	
	<!-- This one in here is responsive menu for tablet and mobiles -->
    <div class="responsive-navigation visible-sm visible-xs">
        <a href="#" class="menu-toggle-btn">
            <i class="fa fa-bars fa-2x">Gestu Hotel</i>
        </a>
        <div class="navigation responsive-menu">
            <ul>
                <li class="home"><a href="#templatemo">Home</a></li>
	            <li class="about"><a href="#about">A Propos de nous</a></li>
	            <li class="portfolio"><a href="#portfolio">Hôtels</a></li>
	            <li class="contact"><a href="#contact">Contact</a></li>
	            <li><a href="/login" class="external">Connexion</a></li>
            </ul> <!-- /.main_menu -->
        </div> <!-- /.responsive_menu -->
    </div> <!-- /responsive_navigation -->

	<div id="main-sidebar" class="hidden-xs hidden-sm">
		<div class="logo">
			<a href="#"><h1>Gestu Hotel</h1></a>
			<span>Platform De Réservation Hôtelière</span>
		</div> <!-- /.logo -->

		<div class="navigation">
	        <ul class="main-menu">
	            <li class="home"><a href="#templatemo">Home</a></li>
	            <li class="about"><a href="#about">A Propos de nous</a></li>
	            <li class="portfolio"><a href="#portfolio">Hôtels</a></li>
	            <li class="contact"><a href="#contact">Contact</a></li>
	            <li><a href="/login" class="external">Connexion</a></li>
	        </ul>
		</div> <!-- /.navigation -->

	</div> <!-- /#main-sidebar -->

	<div id="main-content">

		<div id="templatemo">
			<div class="main-slider">
				<div class="flexslider">
					<ul class="slides">
                    
						<li>
							<div class="slider-caption">
								<h2>Hôtel 5 Etoiles</h2>
								<p>Réserver des chambres dans dans des hôtels 5 Etoiles avec un confort agréable!</p>
								<a href="#" class="largeButton homeBgColor">Read More</a>
							</div>
							<img src="images/hotel-5-etoile.jpeg" alt="hotel 5 etoiles">
						</li>
                        
						<li>
							<div class="slider-caption">
								<h2>Hôtel 4 Etoiles</h2>
								<p>Réserver des chambres dans dans des hôtels 5 Etoiles avec un confort agréable!</p>
								<a href="#" class="largeButton homeBgColor">Details</a>
							</div>
							<img src="images/hotel-4-etoiles.jpg" alt="hotel 4 etoiles">
						</li>
                        
                        <li>
							<div class="slider-caption">
								<h2>Hôtel 3 Etoiles</h2>
								<p>Réserver des chambres dans dans des hôtels 5 Etoiles avec un confort agréable!</p>
								<a href="#" class="largeButton homeBgColor">Downloads</a>
							</div>
							<img src="images/hotel-3-etoiles.jpg" alt="Hôtel 3 étoiles">
						</li>
                        
					</ul>
				</div>
			</div>
			@if(Session::has('success'))
    <div class="alert alert-success">
        {{Session::get('success')}}
    </div>
		@endif
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="welcome-text">
							<h2>Bienvenue sur <b>Gestu Hotel</b></h2>
							<p>Platform de Gestion de plusieurs hôtels dans le Cameroun, Ainsi 
                                pour vous déplacement, contactez nous ou faites le choix d'un hôtel dans 
                                sur notre platform puis effectuer une réservation : réservation de salle, chambre
                                suite, restaurant...
                            </p>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- /#sTop -->

		<div class="container-fluid">

			<div id="about" class="section-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>About Us</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="member-item">
							<div class="member-thumb">
								<img src="images/unknow.jpg" alt="girl 1">
								<div class="overlay">
									<ul class="social-member">
										<li><a href="#" class="fa fa-facebook"></a></li>
										<li><a href="#" class="fa fa-twitter"></a></li>
										<li><a href="#" class="fa fa-linkedin"></a></li>
									</ul>
								</div>
							</div>
							<div class="member-content">
								<h4>Manuel Pangop</h4>
								<p>Directeur Marketing</p>
							</div>
						</div>
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
						<div class="member-item">
							<div class="member-thumb">
								<img src="images/unknow.jpg" alt="girl 2">
								<div class="overlay">
									<ul class="social-member">
										<li><a href="#" class="fa fa-facebook"></a></li>
										<li><a href="#" class="fa fa-twitter"></a></li>
										<li><a href="#" class="fa fa-linkedin"></a></li>
									</ul>
								</div>
							</div>
							<div class="member-content">
								<h4>La D</h4>
								<p>Développeur</p>
							</div>
						</div>
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
						<div class="member-item">
							<div class="member-thumb">
								<img src="images/unknow.jpg " alt="girl 3">
								<div class="overlay">
									<ul class="social-member">
										<li><a href="#" class="fa fa-facebook"></a></li>
										<li><a href="#" class="fa fa-twitter"></a></li>
										<li><a href="#" class="fa fa-linkedin"></a></li>
									</ul>
								</div>
							</div>
							<div class="member-content">
								<h4>Dr Zive</h4>
								<p>Développeur</p>
							</div>
						</div>
					</div> <!-- /.col-md-4 -->
                    <div class="col-md-4">
						<div class="member-item">
							<div class="member-thumb">
								<img src="images/unknow.jpg " alt="girl 3">
								<div class="overlay">
									<ul class="social-member">
										<li><a href="#" class="fa fa-facebook"></a></li>
										<li><a href="#" class="fa fa-twitter"></a></li>
										<li><a href="#" class="fa fa-linkedin"></a></li>
									</ul>
								</div>
							</div>
							<div class="member-content">
								<h4>Cathy Tsogny</h4>
								<p>Président Company</p>
							</div>
						</div>
					</div> <!-- /.col-md-4 -->
				</div> <!-- /.row -->
	&nbsp;

			<div id="portfolio" class="section-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>Hôtels</h2>
						</div> <!-- /.section-title -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
				
				
                @foreach($hotels as $hotel)
			
				<div class="row">
					<div class="col-md-4">
						<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/hotel/{{$hotel->nom_img_hotel}}.jpg" alt="{{request('nom_img_hotel')}}">
								<div class="overlay-p">
									<a href="{{url('hotel/'.$hotel->nom_hotel.'/'.$hotel->description_hotel)}}" data-gal="prettyPhoto">
										<i class="fa fa-arrows-alt fa-2x"></i>
									</a>
								</div>
							</div> <!-- /.portfolio-thumb -->
							<h3 class="portfolio-title"><a href="{{url('hotel/'.$hotel->nom_hotel.'/'.$hotel->description_hotel)}}">{{$hotel->description_hotel}}</a></h3>
						</div> <!-- /.portfolio-item -->
					</div> <!-- /.col-md-4 -->
				@endforeach
				
			<div id="contact" class="section-content">
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>Contact Us</h2>
						</div> <!-- /.section-title -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
				<div class="row">
					<div class="col-md-12">
						<div class="map-holder">
							<div class="google-map-canvas" id="map-canvas">
                    		</div>
						</div> <!-- /.map-holder -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
				<form action="{{url('/')}}" method="POST">
					@csrf
				<div class="row contact-form">
					<div class="col-md-4">
						<label for="name-id" class="required">Name:</label>
						<input name="nom" type="text" id="name-id" maxlength="40">
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
						<label for="email-id" class="required">Email:</label>
						<input name="email" type="text" id="email-id" maxlength="40">
					</div> <!-- /.col-md-4 -->
					<div class="col-md-4">
						<label for="subject-id">Subject:</label>
						<input name="sujet" type="text" id="subject-id" maxlength="60">
					</div> <!-- /.col-md-4 -->
					<div class="col-md-12">
						<label for="message-id" class="required">Message:</label>
						<textarea name="message" id="message-id" rows="6"></textarea>
					</div> <!-- /.col-md-12 -->
					<div class="col-md-12">
						<div class="submit-btn">
						<input type="submit" class="largeButton contactBgColor" value="Envoyer Message">							</div> <!-- /.submit-btn -->
					</div> <!-- /.col-md-12 -->
				</div>
			</div> 
								@if($errors->any())
									@foreach($errors->all() as $error)
										<div class="text-red-500">{{$error}}</div>
									@endforeach
								@endif

				</form>
		</div> <!-- /.container-fluid -->

		<div class="site-footer">
			<div class="first-footer">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="social-footer">
								<ul>
									<li><a href="#" class="fa fa-facebook"></a></li>
									<li><a href="#" class="fa fa-twitter"></a></li>
									<li><a href="#" class="fa fa-dribbble"></a></li>
									<li><a href="#" class="fa fa-linkedin"></a></li>
									<li><a href="#" class="fa fa-rss"></a></li>
								</ul>
							</div> <!-- /.social-footer -->
						</div> <!-- /.col-md-12 -->
					</div> <!-- /.row -->
				</div> <!-- /.container-fluid -->
			</div> <!-- /.first-footer -->
			<div class="bottom-footer">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6">
							<p class="copyright">Copyright © 2084 <a href="#">Your Company Name</a>
                            </p>
						</div> <!-- /.col-md-6 -->
						<div class="col-md-6 credits">
							<p><!-- Design: <a rel="nofollow" href="http://www.templatemo.com/tm-394-sonic" target="_parent">Sonic</a> --></p>
						</div> <!-- /.col-md-6 -->
					</div> <!-- /.row -->
				</div> <!-- /.container-fluid -->
			</div> <!-- /.bottom-footer -->
		</div> <!-- /.site-footer -->

	</div> <!-- /#main-content -->

	<!-- JavaScripts -->
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/jquery.singlePageNav.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/custom.js"></script>
	<script>
		$(document).ready(function(){
			$("a[data-gal^='prettyPhoto']").prettyPhoto({hook: 'data-gal'});
		});

        function initialize() {
          var mapOptions = {
            zoom: 13,
            center: new google.maps.LatLng(40.7809919,-73.9665273)
          };

          var map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);
        }

        function loadScript() {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
              'callback=initialize';
          document.body.appendChild(script);
        }

        window.onload = loadScript;
    </script>
<!-- templatemo 394 sonic -->
</body>
</html>