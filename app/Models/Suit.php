<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Suit extends Model
{
    use HasFactory;
    protected $fillable = ['type_suite', 'description_suite', 'prix_suite', 'nom_photo_suite', 'id_hotel'];
}
