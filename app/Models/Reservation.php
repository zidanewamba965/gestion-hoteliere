<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $fillable = ['email','name','nom_hotel', 'type_chambre', 'nombre_personne', 'num_tel', 'duree_sejour', 'date_arrivee', 'mode_payement'];
}
