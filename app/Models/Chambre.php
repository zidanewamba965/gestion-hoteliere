<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Hotel;

class Chambre extends Model
{
    use HasFactory;
    protected $fillable = ['type_chambre', 'description_chambre', 'nom_photo_chambre', 'nom_hotel', 'id_hotel'];
    
    public function chambres () {
        return $this->hasMany(Chambre::class);
    }
}
