<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reservation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class FaireReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fairereservations = DB::table('reservations')->where('email', Auth::user()->email);

        $fairereservations = $fairereservations->get();


        return view('dashboard', compact('fairereservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('fairereservation');   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'email' => 'required',
             'name' => 'required',
             'nom_hotel' => 'required',
             'type_chambre'=>'required',
             'nombre_personne'=>'required|min:1|max:10',
             'duree_sejour'=>'required',
             'date_arrivee'=>'required',
             'num_tel' => 'required|min:0',
             'mode_payement' => 'required',
             
        ]);

        Reservation::create([
            'email' => $request -> email,
            'name' => $request -> name,
            'nom_hotel' => $request -> nom_hotel,
            'type_chambre' => $request -> type_chambre,
            'nombre_personne' => $request -> nombre_personne,
            'duree_sejour' => $request -> duree_sejour,
            'date_arrivee' => $request -> date_arrivee,
            'num_tel' => $request -> num_tel,
            'mode_payement' => $request -> mode_payement,
            
        ]);

        return redirect('dashboard')->with('success', 'Votre réservation a été enregistrée avec success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $fairereservations = Reservation::findOrFail($id);
        return view('editreservation', compact('fairereservations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes = $request->validate([
            'email' => 'required',
             'name' => 'required',
             'nom_hotel' => 'required',
             'type_chambre'=>'required',
             'nombre_personne'=>'required|min:1|max:10',
             'duree_sejour'=>'required',
             'date_arrivee'=>'required',
             'num_tel' => 'required|min:0',
             'mode_payement' => 'required',
             
        ]);
        $fairereservations = Reservation::findOrFail($id);
            $fairereservations->update($attributes);
            return redirect()->route('dashboard')->with('success','Votre reservation a été modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
