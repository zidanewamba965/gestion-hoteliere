<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255|min:3',
             'nom_hotel'=>'required|max:255|min|:3|unique:posts',
             'type_chambre'=>'required',
             'nombre_personne'=>'required',
             'num_tel'=>'required|unique:posts',
             'duree_sejour'=>'required',
             'date_arrivee'=>'required',
             'mode_payement'=>'mode_payement',
        ];
    }
}
