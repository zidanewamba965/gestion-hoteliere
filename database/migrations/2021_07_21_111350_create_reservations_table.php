<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->string('email') -> references ('email') -> on ('users');
            $table->string('name') -> references ('name') -> on('users');
            $table->string('nom_hotel') -> references ('nom_hotel') -> on ('hotels');
            $table->string('type_chambre') -> references ('type_chambre') -> on ('chambres');
            $table->integer('nombre_personne');
            $table->integer('duree_sejour');
            $table->Date('date_arrivee');
            $table->integer('num_tel');
            $table->string('mode_payement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
