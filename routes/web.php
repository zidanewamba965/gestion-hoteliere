<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\ChambreController;
use App\Http\Controllers\SuiteController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\FaireReservationController;
use App\Http\Controllers\ForumController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/contact', [ContactController::class,  "store"]);

Route::get('/dashboard', [FaireReservationController::class,  "index"])->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/', [HotelController::class, "index"]);
Route::post('/', [HotelController::class,  "store"]);


Route::get('hotel/{nom_hotel}/{description_hotel}', function () {
    return view('hotel');

    
});

Route::get('hotel/{nom_hotel}/{description_hotel}',[ChambreController::class,  "index"]);


Route::get('reservation/{nom_hotel}', function () {
    return view('reservation');

    
});

Route::get('/listehotel', [ReservationController::class,  "index"]);

Route::get('fairereservation/{nom_hotel}', [FaireReservationController::class,  "create"]);

Route::post('fairereservation/{nom_hotel}', [FaireReservationController::class,  "store"]);

Route::get('fairereservation/editreservation', [FaireReservationController::class,  "edit"]);
Route::get('fairereservation/editreservation/{id}', [FaireReservationController::class,  "edit"]);

Route::post('fairereservation/editreservation/{id}', [FaireReservationController::class,  "update"]);

Route::get('forum/{nom_hotel}', [ForumController::class,  "index"]);

Route::post('forum', [ForumController::class,  "store"]);




